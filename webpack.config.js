const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const webpack = require('webpack');

const isProduction = process.env.NODE_ENV === 'production';

const extractCSS = new ExtractTextPlugin('css/[name].css');

const config = {
  entry: {
    index: './src/js/index.js',
    direccion: './src/js/direccion.js',
    admin_anp: './src/js/admin_anp.js',
    administrador: './src/js/administrador.js',
    logout: './src/js/logout.js',
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'js/[name]-[hash].js',
    publicPath: '/',
  },
  devServer: {
    compress: true,
  },
  devtool: isProduction ? 'hidden-source-map' : 'eval-source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.(png|jpg|svg)$/,
        loader: 'file-loader',
        options: {
          name: 'img/[name]-[hash].[ext]',
        },
      },
      {
        test: /\.css$/,
        loader: extractCSS.extract(
          {
            fallback: 'style-loader',
            use: 'css-loader'
          }),
      },
      {
        test: /\.html$/,
        use: {
          loader: 'html-loader',
          options: {
            interpolate: 'require',
            attrs: ['img:src', 'link:href'],
          },
        },
      },
      {
        test: /\.(ttf$|eot|woff2?)$/,
        use: {
          loader: 'file-loader',
          options: {
            name: 'fonts/[name]-[hash].[ext]',
          },
        },
      },
    ],
  },
  plugins: [
    extractCSS,
    new CopyWebpackPlugin([{
      from: 'src/letsencrypt/',
      to: '.well-known/acme-challenge/'
    }]),
    new HtmlWebpackPlugin({
      chunks: ['index'],
      filename: 'index.html',
      template: 'src/html/index.html',
    }),
    new HtmlWebpackPlugin({
      chunks: ['admin_anp'],
      filename: 'admin_anp/index.html',
      template: 'src/html/admin_anp.html',
    }),
    new HtmlWebpackPlugin({
      chunks: ['direccion'],
      filename: 'direccion/index.html',
      template: 'src/html/direccion.html',
    }),
    new HtmlWebpackPlugin({
      chunks: ['administrador'],
      filename: 'administrador/index.html',
      template: 'src/html/administrador.html',
    }),
    new HtmlWebpackPlugin({
      chunks: ['logout'],
      filename: 'logout/index.html',
      template: 'src/html/logout.html',
    }),
  ],
};

// Production plugins
if (isProduction) {
  config.plugins = config.plugins.concat(
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false,
    }),
    new webpack.DefinePlugin({
      'process.env': Object.assign(
        {},
        {
          NODE_ENV: JSON.stringify('production'),
        }
      )}),
    new webpack.optimize.UglifyJsPlugin({
      beautify: false,
      mangle: {
        screw_ie8: true,
        keep_fnames: true,
      },
      compress: {
        warnings: true,
      },
      comments: false,
    }),
  );
}

module.exports = config;
