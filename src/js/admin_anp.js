import '../css/admin_anp.css';
import {redirectIfNotAuth, getAuthData} from './lib/auth';

const apiUrl = 'http://162.243.216.13:8060/api/v1';

redirectIfNotAuth('/');

{
  const form = document.getElementById('admin-apn-form');

  form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const {protected_natural_area: {id: pna_id}} = await getAuthData();

    const postVisitResponse = await fetch(`${apiUrl}/visits/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        date: form.fecha.valueAsDate.toISOString().split('T')[0],
        exonerated: form.exonerados.valueAsNumber,
        foreign: form.extranjeros.valueAsNumber,
        national: form.nacionales.valueAsNumber,
        protected_natural_area: pna_id,
      }),
    });

    const messageNode = document.querySelector('#admin-apn-form .message')

    if (! postVisitResponse.ok) {
      messageNode.innerText = 'Error enviando petición';
      messageNode.className = 'error';
      return;
    }

    messageNode.innerText = 'Envio exitoso';
    messageNode.className = 'success';
    form.reset();
  });
}
