import '../css/direccion.css';
import Chart from 'chart.js';
import {redirectIfNotAuth} from './lib/auth';
import {insertResumenAnual} from './lib/table';

redirectIfNotAuth('/');

const apiUrl = 'http://162.243.216.13:8060/api/v1';

function injectChart(options, id) {
  const ctx = document.getElementById(id).getContext('2d');
  return new Chart(ctx, options);
}


(async () => {
  const visitasResponse = await fetch(
    `${apiUrl}/reports/chart/visits?month=${new Date().getUTCMonth() + 1}`);

  if(visitasResponse.ok) {
    const {exonerated, foreign, national} = await visitasResponse.json();
    injectChart({
      type: "doughnut",
      data : {
        datasets :[{
     	    data : [foreign, national, exonerated],
     	    backgroundColor: ["#F7464A","#46BFBD","#FDB45C",],
        }],
        labels : ["Extranjeros","Nacional","Exonerados",]
      },
      options : {
        responsive : true,
      }
    }, 'cuadro_01');
  }
})();

injectChart({
  type: "doughnut",
  data : {
    datasets :[{
     	data : [74,26,],
     	backgroundColor: ["#F7464A","#46BFBD",],
    }],
    labels : ["Pagantes","No pagantes",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_02')

injectChart({
  type: "doughnut",
  data : {
    datasets :[{
     	data : [31,65,4,],
     	backgroundColor: ["#F7464A","#46BFBD","#FDB45C",],
    }],
    labels : ["Extranjeros","Nacional","Exonerados",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_04');

// Tabla visitas

(async () => {
  const visitasResponse = await fetch(
    `${apiUrl}/reports/visits-anual?month=${new Date().getUTCMonth() + 1}`);

  if (! visitasResponse.ok) {
    return;
  }

  insertResumenAnual(
    document.getElementById('tabla-visitas-container'),
    (await visitasResponse.json()).list_anp);
})();

injectChart({
  type: "doughnut",
  data : {
    datasets :[{
      data : [34,63,3,],
      backgroundColor: ["#F7464A","#46BFBD","#FDB45C",],
    }],
    labels : ["Extranjeros","Nacional","Exonerados",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_04-1')

injectChart({
  type: "doughnut",
  data : {
    datasets :[{
      data : [98,2,],
      backgroundColor: ["#F7464A","#FDB45C",],
    }],
    labels : ["Por Boletaje","Por OD",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_06');

injectChart({
  type: "bar",
  data : {
    datasets :[{
      data : [1047383,1100486],
      backgroundColor: ["#F7464A","#FDB45C",],
    }],
    labels : ["Set 2016","Set 2017",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_08');

injectChart({
  type: "bar",
  data : {
    datasets :[{
      data : [2702784,2233675],
      backgroundColor: ["#F7464A","#FDB45C",],
    }],
    labels : ["Set 2016","Set 2017",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_11');

injectChart({
  type: "bar",
  data : {
    datasets :[{
      data : [13228310,16263241],
      backgroundColor: ["#F7464A","#FDB45C",],
    }],
    labels : ["Suma de Año 2016","Suma de Año 2017",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_13');

injectChart({
  type: "bar",
  data : {
    datasets :[{
      data : [1408874,1620338],
      backgroundColor: ["#F7464A","#FDB45C",],
    }],
    labels : ["Set-2016","Set-2017",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_14');

injectChart({
  type : "line",
  data : {
    labels : [
      "S.H. Machupicchu",
      "RNSIIPG-Ballestas",
      "P.N. Huascarán",
      "R.N. Paracas",
      "R.N. Tambopata",
    ],
    datasets : [{
      label : "Ene-Set 2016",
      backgroundColor : "rgba(220,220,220,0.5)",
      data : [3771804,2522938,1964468,1811390,1537005]
    },
                {
                  label : "Ene-Set 2017",
                  backgroundColor : "rgba(151,187,205,0.5)",
                  data : [6437015,2651298,2004210,1940824,1575910]
                },
               ]
  },
  options : {
    elements : {
      rectangle : {
        borderWidth : 1,
        borderColor : "rgb(0,255,0)",
        borderSkipped : 'bottom'
      }
    },
    responsive : true,
  }
}, 'cuadro_16-1');

injectChart({
  type: "doughnut",
  data : {
    datasets :[{
      data : [81,19],
      backgroundColor: ["#F7464A","#FDB45C",],
    }],
    labels : ["Completado","Objetivo",]
  },
  options : {
    responsive : true,
  }
}, 'cuadro_15');

injectChart({
  type : "line",
  data : {
    labels : ["R.N. Pacaya Samiria", "P.N. Tingo María", "R.N. Lachay", "P.N. Manu", "RNSIIPG-Palomino","SH Bosque de Pómac"],
    datasets : [{
      label : "Ene-Set 2016",
      backgroundColor : "rgba(220,220,220,0.5)",
      data : [525989,374289,233280,193611,107234,77802]
    },
    			      {
    				      label : "Ene-Set 2017",
    				      backgroundColor : "rgba(151,187,205,0.5)",
    				      data : [534089,425714,294253,196838,151024,49674]
    			      },
    			     ]
  },
  options : {
    elements : {
    	rectangle : {
    		borderWidth : 1,
    		borderColor : "rgb(0,255,0)",
    		borderSkipped : 'bottom'
    	}
    },
    responsive : true,
  }
}, 'cuadro_16-2');

//------------------------------------------

injectChart({
  type : "line",
  data : {
    labels : ["SN Ampay", "S.N. Lagunas de Mejía", "P.N. Bahuaja Sonene", "P.N. Yanachaga Chemillén", "SN Los Manglares de Tumbes","RNSIIPG-Punta San Juan","R.N. Junín","S.H. Chacamarca"],
    datasets : [{
      label : "Ene-Set 2016",
    	backgroundColor : "rgba(220,220,220,0.5)",
    	data : [12986,11847,10470,9133,4469,3378]
    },
    			      {
    				      label : "Ene-Set 2017",
    				      backgroundColor : "rgba(151,187,205,0.5)",
    				      data : [13200,12318,11250,9413,4847,1474,1231,575]
    			      },
    			     ]
  },
  options : {
    elements : {
    	rectangle : {
    		borderWidth : 1,
    		borderColor : "rgb(0,255,0)",
    		borderSkipped : 'bottom'
    	}
    },
    responsive : true,
  }
}, 'cuadro_16-3');
