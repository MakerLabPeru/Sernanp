export const monthShortNameArray = [
  'Ene', 'Feb', 'Mar',
  'Abr', 'May', 'Jun',
  'Jul', 'Ago', 'Sep',
  'Oct', 'Nov', 'Dic',
]

export const monthFullNameArray = [
  'Enero', 'Febrero', ' Marzo',
  'Abril', 'Mayo', 'Junio',
  'Julio', 'Agosto', 'Setiembre',
  'Octubre', 'Noviembre', 'Diciembre',
]

export function insertResumenAnual(node, data) {
  console.log(data);
  const fragment = document.createDocumentFragment();

  const date = new Date();
  const month = monthShortNameArray[date.getUTCMonth()];
  const year = date.getUTCFullYear();

  const heading = document.createElement('h6');
  heading.textContent = (
    'Cuadro N° 03: Visitas realizadas a las ANP ' +
      `${monthFullNameArray[date.getUTCMonth()]} (${year-1}-${year})`);
  fragment.appendChild(heading);

  const table = document.createElement('table');
  table.className = 'tabla';
  fragment.appendChild(table);

  const tableHead = document.createElement('thead');
  const tableBody = document.createElement('tbody');
  const tableFooter = document.createElement('tfoot');
  table.appendChild(tableHead);
  table.appendChild(tableBody);
  table.appendChild(tableFooter);

  // Header
  {
    const row = document.createElement('tr');
    tableHead.appendChild(row);

    ['N°', 'Areas Naturales Protegidas', `${month}-${year-1}`,
     `${month}-${year}`, 'Variación'].forEach(
       (header, i) => {
         const cell = document.createElement('th');
         cell.textContent = header;
         if (i === 0) {
           cell.scope = 'row';
         }
         row.appendChild(cell);
       });
  }

  const rowsArray = data.map(({
    this_year_exonerated, this_year_foreign, this_year_national,
    last_year_exonerated, last_year_foreign, last_year_national,
    anp,
  }, i) => {
    const total = this_year_national + this_year_foreign + this_year_exonerated;
    const last_total = last_year_national + last_year_foreign + last_year_exonerated;

    let variation;
    if (last_total === 0) {
      if (total > 0) {
        variation = 100;
      } else {
        variation = 0;
      }
    } else {
      variation = parseInt((total - last_total)/ last_total*100);
    }

    return [i+1, anp, last_total, total, variation];
  })

  // Populate body
  rowsArray.forEach((rowData) => {
    const row  = document.createElement('tr');
    tableBody.appendChild(row);

    // Populate row
    [...rowData.slice(0, -1), `${rowData[4]}%`].forEach(
      (text, j) => {
        let tagName = 'td';
        if(j === 0) {
          tagName = 'th'
        }

        const cell = document.createElement(tagName);
        cell.textContent = text;
        row.appendChild(cell);
      });
  })

  // Populate footer
  {
    const row = document.createElement('tr');
    tableFooter.appendChild(row);

    const totalArray = rowsArray.reduce(
      ([t1, t2], [_, _2, t1x, t2x, ..._3]) => [t1 + t1x, t2 + t2x],
      [0,0]
    )

    const totalVariation = parseInt((totalArray[1] - totalArray[0])/totalArray[0]*100);

    ['Total Visitantes', ...totalArray, `${totalVariation}%`].forEach(
      (text, i) => {
        let tagName  = 'td';
        if (i === 0) {
          tagName = 'th';
        }
        const cell = document.createElement(tagName);
        cell.textContent = text;
        if (i == 0) {
          cell.colSpan = '2';
        }
        row.appendChild(cell);
      });
  }

  node.appendChild(fragment);
}
