import localForage from "localforage";

const store = localForage.createInstance({
  name: 'sernamp-auth',
});

export function auth(authData) {
  return store.setItem('authData', authData);
}

export function deauth() {
  return store.removeItem('authData');
}

export function getAuthData() {
  return store.getItem('authData');
}

export async function isAuthenticated() {
  const authData = await getAuthData();
  return authData !== null;
}

export async function redirectIfNotAuth(url) {
  if (!await isAuthenticated() && typeof window !== 'undefined') {
    window.location.replace(url);
  }
}

export function redirectToRoleDashboard(role) {
  let path;
  switch(role) {
    case 'admin':
      path = '/administrador'; break;
    case 'boss':
      path = '/direccion'; break;
    default:
      path = '/admin_anp';
  }

  window.location.href = path;
}
