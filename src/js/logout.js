import {deauth} from './lib/auth';

(async () => {
  await deauth();

  window.location.replace('/');
})()

