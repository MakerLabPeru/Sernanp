import '../css/login.css';
import 'isomorphic-fetch';
import {auth, redirectToRoleDashboard} from './lib/auth';

const apiUrl = 'http://162.243.216.13:8060/api/v1';

{
  const form = document.getElementById('login-form');

  form.addEventListener('submit', async (event) => {
    event.preventDefault();

    const authDataResponse = await fetch(`${apiUrl}/api-token-auth/`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: form.correo.value,
        password: form.clave.value,
      }),
    });

    if (! authDataResponse.ok) {
      const messageNode = document.querySelector('#login-form .message');
      messageNode.innerText = 'Credenciales incorrectas';
      return form.reset();
    }

    const authData = await authDataResponse.json();

    auth(authData);

    const {account_level: role} = authData;

    redirectToRoleDashboard(role);
  })
}


